# Global variables
SCRIPT_VERSION=1.0.1
CLI_TITLE="First Factory's Install-Tools Command Line"

# There can be more than one parameter so everything is separated in labels to create iterations
for param in "$@"
do
    case "$param" in
        "-v" | "-version")
            echo ${SCRIPT_VERSION}
            ;;
        
        "-i" | "-install")
            echo Installing Homebrew
            /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
            ;;

        "-ff-user")
            echo Installing basic applications
            brew install --cask pritunl
            brew install --cask slack
            brew install --cask zoom
            brew install --cask microsoft-teams
            brew install --cask google-chrome
            ;;
        
        "-dev")
            echo Installing DEV 1
            brew install nvm
            brew install git
            brew install --cask visual-studio-code
            brew install --cask postman
            ;;

        "-h" | "-help")
            printf "\n${CLI_TITLE} (${SCRIPT_VERSION})\n"
            printf "Usage: install-tools [options]\n"
            printf "options:\n"
            printf "\t-v              Display ${CLI_TITLE} version in use.\n"
            printf "\t-h              Display command line help.\n"
            printf "\t-install        Install Homebrew.\n"
            printf "\t-ff-user        Install all must have tools for an FF employee.\n"
            printf "\t-dev            Install all tools for a developer.\n"
            ;;
    esac
# End of loop
done

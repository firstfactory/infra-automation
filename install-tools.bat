@echo off

:: Global variables
set SCRIPT_VERSION=1.0.1
set CLI_TITLE=First Factory's Install-Tools Command Line

:: There can be more than one parameter so everything is separated in labels to create iterations
:top
:: Final condition, if the param %1 has no value, it's the end of the parameters
if (%1) == () goto :end

:: If no params are passed, help option is displayed
if "%1" == "" goto :help

:: Option mapping
if %1 == -v goto :version
if %1 == -version goto :version
if %1 == -h goto :help
if %1 == -help goto :help
if %1 == -dev-all goto :dev1
if %1 == -dev1 goto :dev1
if %1 == -dev2 goto :dev2
if %1 == -dev3 goto :dev3
if %1 == -git goto :git
if %1 == -vscode goto :vscode
if %1 == -sublime goto :sublime
if %1 == -workbench goto :workbench
if %1 == -i goto :install
if %1 == -install goto :install

:: If the param is no recognized, an alert and help option are displayed
echo.
echo The option '%1' is not recognized
echo.
goto :help

:: In case Chocolatey must be installed
:install
echo Installing Chocolatey
@echo on
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
@echo off
goto :next

:: In case DEV1 pack must be installed
:dev1
echo Installing DEV 1
choco install nvm -y --force
choco install nodejs -y --force
choco install vscode -y --force
choco install postman -y --force
:: If just DEV1 has been flagged then continue with next param
:: Otherwise, continue with DEV2 pack instaltion
if not %1 == -dev-all goto :next

:: In case DEV2 pack must be installed
:dev2
echo Installing DEV 2
choco install jdk8 -y --force
choco install intellijidea-community -y --force
choco install postman -y --force
:: If just DEV2 has been flagged then continue with next param
:: Otherwise, continue with DEV3 pack instaltion
if not %1 == -dev-all goto :next

:: In case DEV3 pack must be installed
:dev3
echo Installing DEV 3
choco install visualstudio2019community -y --force
goto :next

:git
echo Installing Git
choco install git
goto :next

:: In case Visual Studio Code must be installed
:vscode
echo Installing Visual Studio Code
choco install vscode -y --force
goto :next

:: In case Sublime Text 3 must be installed
:sublime
echo Installing Sublime Text 3
choco install sublimetext3 -y --force
goto :next

:: In case MySQL Workbench must be installed
:workbench
echo Installing MySQL Workbench
choco install mysql.workbench -y --force
goto :next

:: Help option
:help
echo.
echo %CLI_TITLE% (%SCRIPT_VERSION%)
echo.
echo Usage: install-tools [options]
echo.
echo options:
echo    -v, -version    Display %CLI_TITLE% version in use.
echo    -h, -help       Display command line help.
echo    -i, -install    Install Chocolatey.
echo    -dev1           Install NVM with last LTS node version, VSCode, Postman.
echo    -dev2           Install Java SDK to latest version, IntelliJ, Postman.
echo    -dev3           Install Visual Studio 2019 Community Edition.
echo    -dev-all        Install all previous tools.
echo    -vscode         Install Visual Studio Code.
echo    -sublime        Install Sublime Text 3.
echo    -workbench      Install MySQL Workbench.
echo.
goto :next

:: Version option
:version
echo %SCRIPT_VERSION%
goto :next

:next
:: Change to the next parameter
shift
goto :top

:end
